@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{route('users.update', $user->id)}}" method="post">
            {{csrf_field()}}
            {{method_field('PATCH')}}
            <h1>{{$user->name}}</h1>
            <inputfield name="name"
                        label="Name"
                        value="{{old('name', $user->name)}}"
                        :errors="{{json_encode($errors->get('name'))}}"></inputfield>
            <h2>Change password</h2>
            <inputfield type="password"
                        name="current_password"
                        label="Current password"
                        :errors="{{json_encode($errors->get('current_password'))}}"></inputfield>
            <inputfield type="password"
                        name="new_password"
                        label="New password"
                        :errors="{{json_encode($errors->get('new_password'))}}"></inputfield>
            <inputfield type="password"
                        name="new_password_confirmation"
                        label="Repeat password"
                        :errors="{{json_encode($errors->get('new_password_confirmation'))}}"></inputfield>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
@stop
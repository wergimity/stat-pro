<?php /** @var \ESportsClient\Result\Tournament $tournament */ ?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{$tournament->name}}</h1>
        @if($tournament->startsAt)
            <p>
                <strong>Starts at:</strong>
                {{$tournament->startsAt->format('Y-m-d H:i:s')}}
            </p>
        @endif
        @if($tournament->endsAt)
            <p>
                <strong>Ends at:</strong>
                {{$tournament->endsAt->format('Y-m-d H:i:s')}}
            </p>
        @endif

        @if($tournament->teams)
            <h2>Teams</h2>
            <ul class="list-group">
                @foreach($tournament->teams as $team)
                    <li class="list-group-item">
                        @if(Auth::check())
                            <a class="pull-right"
                               title="Team tournament statistics"
                               href="{{route('tournaments.teams.statistics.index', [$tournament->id, $team->id])}}">
                                <span class="glyphicon glyphicon-stats"></span>
                            </a>
                        @endif
                        {{$team->name}}
                    </li>
                @endforeach
            </ul>
        @endif
    </div>
@stop
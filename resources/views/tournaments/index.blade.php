<?php /** @var $tournaments \ESportsClient\Result\Tournament[] */ ?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>@lang('app.tournament-list')</h1>
        <div class="list-group">
            @foreach($tournaments as $tournament)
                <a class="list-group-item" href="{{route('tournaments.show', $tournament->id)}}">
                    <span class="badge" title="@lang('app.tournament-team-count')">
                        {{$tournament->teams ? count($tournament->teams) : 0}}
                    </span>
                    <h4 class="list-group-item-heading">
                        {{$tournament->name}}
                    </h4>
                    @if($tournament->startsAt)
                        <p class="list-group-item-text">
                            {{$tournament->startsAt->format('Y-m-d')}}
                        </p>
                    @endif
                </a>
            @endforeach
        </div>
        @if(Auth::check())
            <nav aria-label="...">
                <ul class="pager">
                    @if($page > 1)
                        <li class="previous">
                            <a href="{{route('tournaments.index', ['page' => $page-1])}}">
                                <span aria-hidden="true">&larr;</span>
                                Older
                            </a>
                        </li>
                    @endif
                    @if(count($tournaments) === $limit)
                        <li class="next">
                            <a href="{{route('tournaments.index', ['page' => $page+1])}}">
                                Newer
                                <span aria-hidden="true">&rarr;</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </nav>
        @endif
    </div>
@stop
@extends('layouts.app')

@section('content')
    <div class="container">
        <ul class="nav nav-pills">
            <li class="{{in_array(Route::current()->getName(), ['admin.index', 'admin.api.edit']) ? 'active' : ''}}">
                <a href="{{route('admin.api.edit')}}">API</a>
            </li>
        </ul>
    </div>
@stop
<?php /** @var $team \ESportsClient\Result\Team */ ?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{$team->name}} statistics</h1>
        @foreach($team->players as $player)
            <div>
                <h3>{{$player->name}}</h3>
                @if($player->statistics)
                    <statistic v-bind="{{json_encode($player->statistics)}}"></statistic>
                @else
                    <span class="label label-default">Unknown</span>
                @endif
            </div>
        @endforeach
    </div>
@stop
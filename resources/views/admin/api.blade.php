@extends('layouts.admin')

@section('content')
    @parent

    <div class="container">
        <h1>API settings</h1>
        <form action="{{route('admin.api.update')}}" method="post">
            {{csrf_field()}}
            {{method_field('PATCH')}}
            <label class="checkbox">
                <checkbox name="esports[enabled]"
                          label="Enabled"
                          :checked="{{ old('esports.enabled', config('esports.enabled')) ? 'true' : 'false' }}"></checkbox>
            </label>
            <inputfield name="esports[base_uri]"
                        label="Base URI"
                        value="{{old('esports.base_uri', config('esports.base_uri'))}}"></inputfield>
            <inputfield
                    label="Credentials"
                    name="esports[credentials]"
                    value="{{old('esports.credentials', config('esports.credentials'))}}"></inputfield>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
@stop
<?php

return [
    'tournament-list' => 'Upcoming tournaments',
    'tournament-team-count' => 'Teams attending',
];
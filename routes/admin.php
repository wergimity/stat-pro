<?php

namespace App\Http\Controllers\Admin;

use Route;

Route::get('/', ApiController::class . '@edit')->name('admin.index');

// Api routes
Route::get('/api', ApiController::class . '@edit')->name('admin.api.edit');
Route::patch('/api', ApiController::class . '@update')->name('admin.api.update');
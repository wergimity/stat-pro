<?php

namespace App\Http\Controllers\Api;

use Route, Illuminate\Http\Request;

Route::get('/me', function (Request $request) {
    return $request->user();
});

Route::resource('users', UsersController::class);

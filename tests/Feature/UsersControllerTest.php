<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UsersControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testEditPage()
    {
        $user = factory(User::class)->create();

        $response = $this
            ->actingAs($user)
            ->get(route('users.edit', $user->id));

        $response->assertStatus(200);
        $response->assertViewHas('user');
    }

    public function testUpdatePage()
    {
        $user = factory(User::class)->create();

        $response = $this
            ->actingAs($user)
            ->patch(route('users.update', $user->id), ['name' => 'Updated']);

        $response->assertRedirect('/users/1/edit');
    }
}

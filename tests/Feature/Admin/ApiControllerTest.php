<?php

namespace Tests\Feature\Admin;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApiControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testEdit()
    {
        $user = factory(User::class)->create();

        $this->get('/admin/api')->assertRedirect('/login');

        auth()->login($user);

        $this->get('/admin/api')->assertStatus(403);

        $user->is_admin = true;

        $this->get('/admin/api')->assertStatus(200);
    }

    public function testUpdate()
    {
        $user = factory(User::class)->create();
        $user->is_admin = true;
        auth()->login($user);

        $this->patch(route('admin.api.update', [
            'esports' => [
                'enabled' => 'on',
                'base_uri' => 'http://google.com',
                'credentials' => '',
            ],
        ]));

        $this->assertDatabaseHas('config', ['key' => 'esports.enabled']);
        $this->assertDatabaseHas('config', ['key' => 'esports.base_uri']);
        $this->assertDatabaseHas('config', ['key' => 'esports.credentials']);
    }
}

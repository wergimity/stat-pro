<?php

namespace Tests\Feature;

use App\Models\User;
use App\Validators\ValidPassword;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ValidPasswordValidatorTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testValidator()
    {
        /** @var ValidPassword $validator */
        $validator = app(ValidPassword::class);
        $user = factory(User::class)->create();

        auth()->login($user);

        $user->password = bcrypt('labas');
        $this->assertTrue($validator->check([], 'labas'));
        $this->assertFalse($validator->check([], 'invalid'));
    }

    public function testUnauthorized()
    {
        /** @var ValidPassword $validator */
        $validator = app(ValidPassword::class);

        $this->assertTrue($validator->check([], 'labas'));
    }

    public function testEmpty()
    {
        /** @var ValidPassword $validator */
        $validator = app(ValidPassword::class);
        $user = factory(User::class)->create();

        auth()->login($user);

        $this->assertTrue($validator->check([], ''));
    }
}

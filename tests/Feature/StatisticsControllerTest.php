<?php

namespace Tests\Feature;

use App\Models\User;
use App\Services\StatisticsService;
use ESportsClient\Result\Player;
use ESportsClient\Result\Team;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StatisticsControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTemStatistics()
    {
        $user = factory(User::class)->create();
        $team = new Team((object) [
            'name' => 'Testing',
            'players' => [
                (object) [
                    'name' => 'Player1',
                    'statistics' => (object) ['kills' => 1, 'deaths' => 0],
                ],
            ],
        ]);
        $service = $this->createMock(StatisticsService::class);
        $service->method('tournamentTeam')
            ->with(1, 1)
            ->willReturn($team);
        $this->app->instance(StatisticsService::class, $service);

        $response = $this->actingAs($user)->get(route('tournaments.teams.statistics.index', [1, 1]));

        $response->assertStatus(200);
        $response->assertViewHas('team');
        $this->assertSame($team, $response->original->team);
    }
}

<?php

namespace Tests\Feature;

use App\Models\User;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TournamentsControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndexPage()
    {
        $data = array_fill(0, 10, ['id' => 1, 'name' => 'Tournament']);
        $this->mockApiClient([new Response(200, [], json_encode($data))]);

        $response = $this->get(route('tournaments.index'));

        $response->assertStatus(200);
        $response->assertViewHas('tournaments');
    }

    public function testShowPage()
    {
        $data = ['id' => 1, 'name' => 'Tournament'];
        $this->mockApiClient([new Response(200, [], json_encode($data))]);

        $response = $this->get(route('tournaments.show', 1));

        $response->assertStatus(200);
        $response->assertViewHas('tournament');
    }

    public function testGuestPagination()
    {
        $this->mockApiClient([new Response(200, [], '[]')]);
        $response = $this->get(route('tournaments.index', ['page' => 2]));
        $this->assertEquals(1, $response->original->page);
    }

    public function testLoggedInPagination()
    {
        $this->mockApiClient([new Response(200, [], '[]')]);

        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get(route('tournaments.index', ['page' => 2]));

        $this->assertEquals(2, $response->original->page);
    }
}

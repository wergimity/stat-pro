<?php

namespace Tests;

use App\Models\User;
use ESportsClient\Client;
use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\TestResponse;
use Psr\Http\Message\ResponseInterface;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    /**
     * @param ResponseInterface[] $responses
     *
     * @return TestCase
     */
    protected function mockApiClient($responses)
    {
        $this->app->extend(Client::class, function (Client $client) use ($responses) {
            $handler = HandlerStack::create(new MockHandler($responses));

            $client->setClient(new Guzzle(compact('handler')));

            return $client;
        });

        return $this;
    }

    protected function openResponse(TestResponse $response)
    {
        file_put_contents('/tmp/last-response', $response->getContent());
        shell_exec('google-chrome /tmp/last-response');
    }

    /**
     * @param bool $admin
     *
     * @return User
     */
    protected function createUser($admin = false)
    {
        return factory(User::class)->create();
    }
}

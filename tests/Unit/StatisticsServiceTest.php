<?php

namespace Tests\Unit;

use App\Services\StatisticsService;
use ESportsClient\Client;
use ESportsClient\Result\Team;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StatisticsServiceTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testService()
    {
        $mock = $this->mockApiClient([
            new Response(200, [], '{"players":[{"id":1}]}'),
            new Response(200, [], '{"kills":1,"deaths":2}'),
        ]);
        /** @var StatisticsService $service */
        $service = app(StatisticsService::class);
        $team = $service->tournamentTeam(1, 1);
        $player = $team->players[0];

        $this->assertInstanceOf(Team::class, $team);
        $this->assertEquals(1, $player->statistics->kills);
        $this->assertEquals(2, $player->statistics->deaths);
    }
}

<?php

namespace Tests\Unit;

use App\Services\DisabledClient;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DisabledClientTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testService()
    {
        $service = new DisabledClient();

        $this->assertNull($service->team(1));
        $this->assertNull($service->tournament(1));
        $this->assertNull($service->player(1));
        $this->assertEmpty($service->tournamentList());
        $this->assertEmpty($service->tournamentTeamList(1));
        $this->assertEmpty($service->tournamentPlayerStatistics(1, 1));
        $this->assertEmpty($service->teamList());
        $this->assertEmpty($service->teamPlayerList(1));
        $this->assertEmpty($service->playerList());
        $this->assertEmpty($service->tournamentStatisticsList(1));
    }
}

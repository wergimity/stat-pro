<?php

namespace App\Services;

use ESportsClient\Client;

class DisabledClient extends Client
{
    /**
     * @param string $uri
     * @param string $resultClass
     *
     * @return null
     */
    protected function resource($uri, $resultClass)
    {
        return null;
    }

    /**
     * @param string $uri
     * @param string $resultClass
     * @param array  $query
     *
     * @return array
     */
    protected function collection($uri, $resultClass, $query = [])
    {
        return [];
    }

}
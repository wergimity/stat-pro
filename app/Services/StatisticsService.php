<?php

namespace App\Services;

use ESportsClient\Client;
use ESportsClient\Result\Team;

class StatisticsService
{
    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param int $tournamentId
     * @param int $teamId
     *
     * @return Team
     */
    public function tournamentTeam($tournamentId, $teamId)
    {
        $team = $this->client->team($teamId);

        foreach ($team->players as $player) {
            $player->statistics = $this->client->tournamentPlayerStatistics($tournamentId, $player->id);
        }

        return $team;
    }
}
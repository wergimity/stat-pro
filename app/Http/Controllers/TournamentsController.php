<?php

namespace App\Http\Controllers;

use ESportsClient\Client;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class TournamentsController extends Controller
{
    public function index(Client $client, Guard $auth, Request $request)
    {
        $limit = 10;
        $page = $auth->check() ? $request->get('page', 1) : 1;
        $tournaments = $client->tournamentList($page);

        return view('tournaments.index', compact('tournaments', 'page', 'limit'));
    }

    public function show($id, Client $client)
    {
        $tournament = $client->tournament($id);

        return view('tournaments.show', compact('tournament'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Services\StatisticsService;
use Illuminate\Http\Request;

class StatisticsController extends Controller
{
    public function index($tournament, $team, StatisticsService $service)
    {
        $team = $service->tournamentTeam($tournament, $team);

        return view('statistics.index', compact('team'));
    }
}
<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateProfileRequest;
use App\Models\User;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    public function update(UpdateProfileRequest $request, Hasher $hash)
    {
        /** @var User $user */
        $user = auth()->user();

        $user->name = $request->name;
        $user->password = bcrypt($request->new_password);
        $user->save();

        return redirect()
            ->route('users.edit', $user->id)
            ->with('success_message', 'Profile updated successfully!');
    }
}

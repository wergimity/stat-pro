<?php

namespace App\Http\Controllers\Admin;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    public function edit()
    {
        return view('admin.api');
    }

    public function update(Request $request)
    {
        $values = array_dot($request->only('esports'));

        foreach ($values as $key => $value) {
            DB::table('config')->updateOrInsert(compact('key'), compact('key', 'value'));
        }

        return redirect()
            ->route('admin.api.edit')
            ->with('success_message', 'Settings updated successfully!');
    }
}

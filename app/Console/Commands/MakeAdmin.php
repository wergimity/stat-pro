<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class MakeAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:make {user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make user an admin';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::findOrFail($this->argument('user'));

        $user->is_admin = true;
        $user->save();

        $this->info("User $user->name updated successfully!");
    }
}

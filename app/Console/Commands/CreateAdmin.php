<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Validator;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create admin user';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->askWhileInvalid('Name', 'required|string|max:255');
        $email = $this->askWhileInvalid('Email', 'required|email|unique:users,email');
        $password = $this->secret('Password', 'required|min:5');
        $admin = $this->confirm('Admin?');

        $user = new User();

        $user->name = $name;
        $user->email = $email;
        $user->password = bcrypt($password);
        $user->is_admin = $admin;

        $user->save();
        $this->info('User created successfully!');
    }

    protected function askWhileInvalid($question, $rules)
    {
        $value = $this->ask($question);
        $validator = Validator::make(compact('value'), ['value' => $rules]);
        while ($validator->fails()) {
            foreach ($validator->getMessageBag()->get('value') as $message) {
                $this->error($message);
            }
            $value = $this->ask($question);
            $validator = Validator::make(compact('value'), ['value' => $rules]);
        }

        return $value;
    }
}

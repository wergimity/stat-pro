<?php

namespace App\Validators;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Hashing\Hasher;

class ValidPassword
{
    /**
     * @var Guard
     */
    private $auth;
    /**
     * @var Hasher
     */
    private $hash;

    public function __construct(Hasher $hash, Guard $auth)
    {
        $this->hash = $hash;
        $this->auth = $auth;
    }

    public function check($attributes, $value)
    {
        if (!$this->auth->check()) {
            return true;
        }

        if (!$value) {
            return true;
        }

        return $this->hash->check($value, data_get($this->auth->user(), 'password'));
    }
}
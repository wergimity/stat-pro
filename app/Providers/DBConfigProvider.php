<?php

namespace App\Providers;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Database\Connection;
use Illuminate\Support\ServiceProvider;
use PDOException;

class DBConfigProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Repository $config, Connection $db)
    {
        try {
            if ($db->getSchemaBuilder()->hasTable('config')) {
                $overwrite = $db->table('config')->pluck('value', 'key')->all();

                $config->set($overwrite);
            }
        } catch (PDOException $e) {

        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

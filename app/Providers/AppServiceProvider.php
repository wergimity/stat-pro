<?php

namespace App\Providers;

use App\Services\DisabledClient;
use App\Validators\ValidPassword;
use ESportsClient\Client;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Repository $config)
    {
        Validator::extend('valid_password', ValidPassword::class . '@check');

        if (!$config->get('esports.enabled')) {
            $this->app->bind(Client::class, DisabledClient::class, true);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

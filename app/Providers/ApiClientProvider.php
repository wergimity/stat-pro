<?php

namespace App\Providers;

use ESportsClient\Client;
use Illuminate\Support\ServiceProvider;

class ApiClientProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Client::class, Client::class, true);
    }
}

# Stats PRO app

## Installation
1. Run the following commands:
```bash
git clone https://gitlab.com/wergimity/stat-pro.git
cd stat-pro
composer install
cp .env.example .env
```
2. Then update `.env` file to configure project
3. Run the following commands:
```bash
php artisan key:generate
php artisan migrate
php artisan admin:create
```

## Running project
```bash
php artisan serve
```